package com.example.tarea1;

import Fragments.Content_fragment;
import Fragments.Fragment_list_view_images;
import Fragments.Images;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;


public class MainActivity extends ActionBarActivity{

	Store datos;
	Intent detalle;
	Intent intent; 
	private DrawerLayout drawerlayout;
	private ListView drawerList;
	private String[] drawerOptions;
	private Fragment[] fragments = new Fragment[]{
			new Images(),
			new Content_fragment(),
			new Fragment_list_view_images()};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		drawerList = (ListView)findViewById(R.id.leftDrawer);
		drawerlayout = (DrawerLayout) findViewById(R.id.drawerLayout);
		drawerOptions = getResources().getStringArray(R.array.drawer_options);
		
		drawerList.setAdapter(new ArrayAdapter<String>(this,R.layout.drawer_list_item,drawerOptions));
		
		drawerList.setItemChecked(0, true);
		drawerList.setOnItemClickListener(new DrawerItemClickListener());
		
		FragmentManager manager = getSupportFragmentManager();
		manager.beginTransaction()
						.add(R.id.contentFrame, fragments[0])
						.add(R.id.contentFrame, fragments[1])
						.add(R.id.contentFrame, fragments[2])
						.hide(fragments[1])
						.hide(fragments[2])
						.commit();
		
		
		
		
	}
	
	public void SetContent(int index){
		Fragment toHide = null;
		Fragment toshow = null;
		Fragment toHide2 = null;
		ActionBar actionBar = getSupportActionBar();
		if(index == 0){
			toHide = fragments[1];
			toshow = fragments[0];
			toHide2 = fragments[2];
			actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
		}else
		if(index == 1){
			toHide = fragments[0];
			toshow = fragments[1];
			toHide2 = fragments[2];
//			actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
			
		}else
		if(index==2){
			toHide = fragments[0];
			toHide2 = fragments[1];
			toshow = fragments[2];
			actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
		}
		FragmentManager manager = getSupportFragmentManager();
		manager.beginTransaction().hide(toHide).show(toshow).hide(toHide2).commit();

		drawerList.setItemChecked(index, true);
		drawerlayout.closeDrawer(drawerList);
	}
	
	class DrawerItemClickListener implements ListView.OnItemClickListener{

		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int position,
				long arg3) {
			SetContent(position);
			
		}
		
	}
}
