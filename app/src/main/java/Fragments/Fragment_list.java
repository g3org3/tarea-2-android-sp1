package Fragments;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.example.tarea1.Store;
import com.example.tarea1.DetallesActivity;
import com.example.tarea1.MainActivity;
import com.example.tarea1.Photography;
import com.example.tarea1.R;

import android.content.Intent;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Menu;
import android.widget.AdapterView.OnItemClickListener;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;



public class Fragment_list extends Fragment implements OnItemClickListener {

	private List<String> tiendas = new ArrayList<String>();
	ArrayAdapter adapter;
	Store store;
	Intent detalle;
	private List<Store> detalles = new ArrayList<Store>();
	Intent intent;
	ListView list;

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		try {
			readjsonfile();
		} catch (Exception e) {
			Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
		}
		adapter = new ArrayAdapter(getActivity(), android.R.layout.simple_list_item_1, tiendas);
		list.setAdapter(adapter);
		list.setOnItemClickListener(new OnItemClickListener() {
				  @Override
				  public void onItemClick(AdapterView<?> parent, View view,
				    int position, long id) {
					  if(position == 2){
						  intent = new Intent(getActivity(),
									Photography.class);
						  startActivity(intent);
					  }else{
						  String nombre;
						  String direccion;
						  String telefono;
						  String horario;
						  String website;
						  String email;
						  nombre = tiendas.get(position).toString();
						  direccion = detalles.get(position).getdireccion().toString();
						  telefono = detalles.get(position).gettelefono().toString();
					      horario = detalles.get(position).gethorario().toString();
						  website = detalles.get(position).getwebsite().toString();
						  email = detalles.get(position).getemail().toString();
						  detalle = new Intent(getActivity(),
										DetallesActivity.class);
						  detalle.putExtra("nombre", nombre);
						  detalle.putExtra("direccion", direccion);
						  detalle.putExtra("telefono", telefono);
						  detalle.putExtra("horario", horario);
						  detalle.putExtra("website", website);
						  detalle.putExtra("email", email);
						  startActivity(detalle);
				  	 }
				  }
			});
	}
	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_list,container, false);
		list = (ListView) view.findViewById(R.id.list_tienda);
		return view;
	}


	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getActivity().getMenuInflater().inflate(R.menu.main, menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int item_id = item.getItemId();
		Intent intent;
		if(item_id == R.id.Acerca_de){
			Toast.makeText(getActivity(), "Hecho por: Jorge Adolfo Gonzalez",
	                 Toast.LENGTH_SHORT).show();
		}
		if(item_id == R.id.Listado){
			intent = new Intent(getActivity(),
					MainActivity.class);
			 startActivity(intent);
		}else
		if(item_id == R.id.Salir){
			getActivity().finish();	
			getActivity().moveTaskToBack(true);
		}
		return true;
	}


	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		// TODO Auto-generated method stub
		
	}
	public void listtienda(List<Store> detalles){
		for(int i = 0; i < detalles.size(); i++){
			tiendas.add(detalles.get(i).getName());
		}
	}
	
	public void readjsonfile() throws Exception{
		JSONParser parser = new JSONParser();
		AssetManager assetManager = getActivity().getAssets();
		InputStream input;
		input = assetManager.open("archivo.json");
		int size = input.available();
	    byte[] buffer = new byte[size];
	    input.read(buffer);
	    input.close();
	    String text = new String(buffer, "UTF-8");
	    JSONArray a = (JSONArray) parser.parse(text);
	    for (Object o : a){
	    	JSONObject json_object = (JSONObject) o;
	        String type = (String) json_object.get("type");
	        if(type.equals("store")){
	        	String nombre = (String)json_object.get("name");
	        	String direccion = (String)json_object.get("address");
	        	String telefono = (String)json_object.get("phone");
	        	String horario = (String)json_object.get("hoursOfOperaion");
	        	String url = (String)json_object.get("url");
	        	String email = (String)json_object.get("phone");
	        	store = new Store(nombre,direccion, telefono, horario, url, email);
	      		detalles.add(store);
	        }
	    }
	    listtienda(detalles);
	 }
}
