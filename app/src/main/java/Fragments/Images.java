package Fragments;

import com.example.tarea1.R;

import data.PagerAdapter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class Images extends Fragment {

	ViewPager viewpager;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.images, container, false);
		viewpager = (ViewPager)view.findViewById(R.id.pager);
		return view;
	}
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		PagerAdapter adapter = new PagerAdapter(getChildFragmentManager());
		viewpager.setAdapter(adapter);
		}

	
}
