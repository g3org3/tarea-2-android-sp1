package Fragments;

import com.example.tarea1.Store;
import com.example.tarea1.R;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar.Tab;
import android.support.v7.app.ActionBar.TabListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.example.tarea1.MainActivity;

public class Content_fragment extends Fragment implements TabListener{

		Store datos;
		Intent detalle;
		Intent intent;
		Fragment[] fragments = new Fragment[]{new Fragment_list(), new Fragment_list_view_images()};
		
		
		
		@Override
		public void onActivityCreated(Bundle savedInstanceState) {
			super.onActivityCreated(savedInstanceState);
			ActionBar actionbar = ((MainActivity) getActivity()).getSupportActionBar();
			actionbar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
			
			actionbar.addTab(actionbar.newTab().setText("Listado").setTabListener(this));
			actionbar.addTab(actionbar.newTab().setText("Mapa").setTabListener(this));
			
			FragmentManager manager = getActivity().getSupportFragmentManager();
			manager.beginTransaction().add(R.id.mainContent, fragments[0])
									  .add(R.id.mainContent, fragments[1]).commit();
		
		}
		

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			// TODO Auto-generated method stub
			return inflater.inflate(R.layout.fragment_content, container, false);
		}


		@Override
		public void onTabReselected(Tab arg0, FragmentTransaction arg1) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onTabSelected(Tab tab, FragmentTransaction ft) {
			Fragment toHide = null;
			Fragment toshow = null;
			if(tab.getPosition() == 0){
				toHide = fragments[1];
				toshow = fragments[0];
			} else {
				toHide = fragments[0];
				toshow = fragments[1];
			}
			ft.hide(toHide).show(toshow);
		}

		@Override
		public void onTabUnselected(Tab arg0, FragmentTransaction arg1) {
			// TODO Auto-generated method stub
			
		}
	}
