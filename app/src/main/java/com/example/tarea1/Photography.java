package com.example.tarea1;



import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class Photography extends FragmentActivity {
	
	Intent intent;
	ListView listView;
	ArrayAdapter <String> adapter;
	TextView comment;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_photography);
		comment = (TextView)findViewById(R.id.comment);
		listView = (ListView) findViewById(R.id.listView1);
		adapter = new ArrayAdapter <String>(this, android.R.layout.simple_list_item_1);
		listView.setAdapter(adapter);
		addcomment();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.photography, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int item_id = item.getItemId();
		if(item_id == R.id.social){
			Uri Image = null;
			Image = Uri.parse("@drawable/bmw.jpg");
			Intent shareIntent = new Intent();
			shareIntent.setAction(Intent.ACTION_SEND);
			shareIntent.putExtra(Intent.EXTRA_STREAM, Image);
			shareIntent.setType("image/jpg");
			startActivity(Intent.createChooser(shareIntent, getResources().getText(R.string.action_share)));
		}
		if(item_id == R.id.Acerca_de){
			Toast.makeText(this, "Hecho por: Jorge Adolfo Gonzalez",
	                 Toast.LENGTH_SHORT).show();
		}
		if(item_id == R.id.Listado){
			intent = new Intent(this,
					MainActivity.class);
			 startActivity(intent);
		}else
		if(item_id == R.id.Fotografia){
			intent = new Intent(this,
					Photography.class);
			 startActivity(intent);
		}else
		if(item_id == R.id.Salir){
			finish();	
			moveTaskToBack(true);
		}
		return true;
	}
	
	public void addcomment(){
		Button btncall = (Button)findViewById(R.id.addcomment);
		btncall.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				adapter.add(comment.getText().toString());
				adapter.notifyDataSetChanged();
			}
		});
		
	}
}
