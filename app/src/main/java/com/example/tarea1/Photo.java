package com.example.tarea1;

import java.util.ArrayList;

public class Photo {
	String Url;
	String descripcion;
	ArrayList<String> listado_de_comentarios;
	int num_favoritos;
	
	public Photo(String u, String descrip, ArrayList<String> list, int num_fav){
		Url = u;
		descripcion = descrip;
		listado_de_comentarios = list;
		num_favoritos = num_fav;
	}
	
	public String getUrl(){
		return Url;
	}
	
	public String getDescripcion(){
		return descripcion;
	}
	
	public ArrayList<String> getListado(){
		return listado_de_comentarios;	
	}
	
	public int getFavoritos(){
		return num_favoritos;
	}
}
