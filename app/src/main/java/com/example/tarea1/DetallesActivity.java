package com.example.tarea1;



import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.text.util.Linkify;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class DetallesActivity extends Activity {

	Button llamar;
	Intent llamar_activity;
	String tel_text;
	String name_text = "";
	/**
	 * cambio los textview por los valores que le mando de la otra actividad
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Intent intent = getIntent();
		name_text = intent.getStringExtra("nombre");
		String dir_text = intent.getStringExtra("direccion");
		tel_text = intent.getStringExtra("telefono");
		String shedule_text = intent.getStringExtra("horario");
		String website_text = intent.getStringExtra("website");
		String email_text = intent.getStringExtra("email");
		setContentView(R.layout.activity_detalles);
		TextView nombre = (TextView)findViewById(R.id.name);
		TextView direccion = (TextView)findViewById(R.id.direccion);
		TextView telefono = (TextView)findViewById(R.id.telefono);
		TextView horario = (TextView)findViewById(R.id.horario);
		TextView website = (TextView)findViewById(R.id.website);
		TextView email = (TextView)findViewById(R.id.email);
		nombre.setText(name_text);
		direccion.setText(dir_text);
		Linkify.addLinks(direccion, Linkify.ALL);
		telefono.setText(tel_text);
		Linkify.addLinks(telefono, Linkify.ALL);
		horario.setText(shedule_text);
		Linkify.addLinks(horario, Linkify.ALL);
		website.setText(website_text);
		Linkify.addLinks(website, Linkify.ALL);
		email.setText(email_text);
		Linkify.addLinks(email, Linkify.ALL);
		llamar = (Button)findViewById(R.id.llamar);
		ButtonListener listener = new ButtonListener();
		llamar.setOnClickListener(listener);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.detalles, menu);
		return true;
	}
	
	//  intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:976123123"));
      //createButton("Llamada telef�nica", intent);
	/**
	 * Metodo para Mostrar el teclado numerico con el numero ya marcado.
	 * @author Jorge Adolfo
	 *
	 */
	class ButtonListener implements OnClickListener{
		
		@Override
		public void onClick(View v) {
			if(v.getId() == llamar.getId()){
				llamar_activity = new Intent(Intent.ACTION_DIAL,Uri.parse("tel: " + tel_text));
				startActivity(llamar_activity);
			}
		}
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int item_id = item.getItemId();
		Intent intent;
		if(item_id == R.id.social){
			Intent intent1 = new Intent();
			intent1.setAction(Intent.ACTION_SEND);
			intent1.putExtra(Intent.EXTRA_TEXT, name_text);
			intent1.setType("text/plain");
			startActivity(Intent.createChooser(intent1, getString(R.string.action_share)));
		}
		if(item_id == R.id.Acerca_de){
			Toast.makeText(this, "Hecho por: Jorge Adolfo Gonzalez",
	                 Toast.LENGTH_SHORT).show();
		}
		if(item_id == R.id.Listado){
			intent = new Intent(this,
					MainActivity.class);
			 startActivity(intent);
		}else
		if(item_id == R.id.Fotografia){
			intent = new Intent(this,
					Photography.class);
			 startActivity(intent);
		}else
		if(item_id == R.id.Salir){
			finish();	
			moveTaskToBack(true);
		}
		return true;
	}
}
