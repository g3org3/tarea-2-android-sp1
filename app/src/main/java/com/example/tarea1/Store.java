package com.example.tarea1;

public class Store{
	
	String nombre;
	String direccion;
	String telefono;
	String horario;
	String website;
	String email;

	public Store(String name,String dir,String tel,String schedule, String web_site, String e_mail){
		nombre = name;
		direccion = dir;
		telefono = tel;
		horario = schedule;
		website = web_site;
		email = e_mail;
	}
	
	public String getName(){
		return nombre;
	}
	
	public String getdireccion(){
		return direccion;
	}
	
	public String gettelefono(){
		return telefono;
	}
	
	public String gethorario(){
		return horario;
	}
	
	public String getwebsite(){
		return website;
	}
	
	public String getemail(){
		return email;
	}
}