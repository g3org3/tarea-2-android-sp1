package adapters;

import java.util.ArrayList;
import java.util.Arrays;

import com.example.tarea1.R;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class ListviewAdapter extends BaseAdapter {
	private ArrayList<Integer> imagenes;
	private ArrayList<String>  comentarios;
	private Activity activity;
	
	public ListviewAdapter(ArrayList <Integer> ima, ArrayList <String> com, Activity act){
		imagenes = ima;
		comentarios = com;
		activity = act;
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return imagenes.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = activity.getLayoutInflater().inflate(R.layout.images_comunidad, null);
		TextView comment = (TextView) view.findViewById(R.id.comment);
		ImageView image = (ImageView) view.findViewById(R.id.image);
		comment.setText(comentarios.get(position));
		image.setImageResource(imagenes.get(position));
		return view;
	}

}
