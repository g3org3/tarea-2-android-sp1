package data;

import Fragments.Imagenes_fragment;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.tarea1.R;

public class PagerAdapter extends FragmentPagerAdapter {

	public PagerAdapter(FragmentManager fm) {
		super(fm);
		// TODO Auto-generated constructor stub
	}

	@Override
	public Fragment getItem(int position) {
		int [] array = new int[]{
                R.drawable.audi,
				R.drawable.bmw,
				R.drawable.porsche,
				R.drawable.lamborgini,
				R.drawable.maserati};
		Fragment fragment = new Imagenes_fragment();
		Bundle args = new Bundle();
		args.putInt(Imagenes_fragment.RESOURCE, array[position]);
		fragment.setArguments(args);
		return fragment;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return 5;
	}

}
