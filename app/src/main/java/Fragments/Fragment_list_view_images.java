package Fragments;


import java.util.ArrayList;

import adapters.ListviewAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.MediaStore.Audio.Media;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.example.tarea1.R;
public class Fragment_list_view_images extends Fragment implements OnClickListener{
	ListView list;
    ImageButton buton;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.fragment_listview_images, container,false);
		list = (ListView) view.findViewById(R.id.listView_comunidad);
		buton = (ImageButton)view.findViewById(R.id.imageButton1);
		
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		ArrayList<Integer> imagenes = new ArrayList<Integer>();
		imagenes.add(R.drawable.comentario);
		imagenes.add(R.drawable.imagen);
		imagenes.add(R.drawable.ic_launcher);
		ArrayList<String>  comentarios = new ArrayList<String>();
		comentarios.add("Commentario");
		comentarios.add("lobo");
		comentarios.add("ic launcher");
		ListviewAdapter listadapter = new ListviewAdapter(imagenes, comentarios, getActivity());
		list.setAdapter(listadapter);
		buton.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		startActivity(intent);
		
	}
	
}
